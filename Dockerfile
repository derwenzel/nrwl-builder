FROM docker:latest
LABEL maintainer "konrad koch, konrad@labk.it"

# system utilities
RUN apk update && apk upgrade \
    && apk add --no-cache bash git curl yarn npm openssh openssl ca-certificates wget \
    && update-ca-certificates

RUN npm install -g @angular/cli@12.1.4
RUN npm install -g @nrwl/cli@12.8.0


# cleanup
RUN rm -rf /tmp/*
